import 'dotenv/config'
import {
    Client, REST, Partials,
    GatewayIntentBits, Routes,
    ActivityType, ChannelType, Attachment, AttachmentBuilder, Embed, EmbedBuilder
} from 'discord.js'

import config from './config/config.js'
import { askQuestion } from './chatgpt/chatgpt.js'
import { initDiscordCommands, handle_interaction_ask, handle_interaction_image, handle_interaction_remix } from './discord/discord_commands.js'
import {splitAndSendResponse, MAX_RESPONSE_CHUNK_LENGTH} from './discord/discord_helpers.js'
import { initDashboard } from './dashboard/dasboard.js'
import Canvas from 'canvas'
import TwitchMonitor from './twitch-monitor/twitch-monitor.js'
import MiniDb from './twitch-monitor/minidb.js'
import LiveEmbed from './twitch-monitor/live-embed.js'
import Twit from 'twitter-v2'
import Twitter from "twitter-v2";
import moment from "moment";
import {isSet} from "util/types";

const twitterConf = {
    /*consumer_key: config.get("TWITTER_CONSUMER_KEY"),
    consumer_secret: config.get("TWITTER_CONSUMER_SECRET"),
    access_token_key: config.get("TWITTER_ACCESS_TOKEN_KEY"),
    access_token_secret: config.get("TWITTER_ACCESS_TOKEN_SECRET"),*/
    bearer_token: config.get("TWITTER_BEARER_TOKEN")
}

const twitterClient = new Twitter(twitterConf);

let targetChannels = [];
async function main() {
    await initDiscordCommands();
    initDashboard();

    const client = new Client({
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMembers,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.GuildIntegrations,
            GatewayIntentBits.DirectMessages,
            GatewayIntentBits.DirectMessageTyping,
            GatewayIntentBits.MessageContent,
        ],
        partials: [Partials.Channel]
    });

    client.login(process.env.DISCORD_BOT_TOKEN).catch(e => console.log(chalk.red(e)));

    client.once('ready', () => {
        console.log(`Logged in as ${client.user.tag}!`);
        console.log(new Date())
        client.user.setStatus('online');
        client.user.setActivity(config.get("DEFAULT_ACTIVITY"));
    });

    TwitchMonitor.start();

    let liveMessageDb = new MiniDb('live-messages');
    let messageHistory = liveMessageDb.get('history2') || {};

    TwitchMonitor.onChannelLiveUpdate((streamData) => {
        const isLive = streamData.type === "live";

        StreamActivity.setChannelOnline(streamData);

        const msgFormatted = `${streamData.user_name} ya prendio en Twitch!`;
        const msgEmbed = LiveEmbed.createForStream(streamData);

        let anySent = false;

        let discordChannel = client.channels.cache.get(config.get("DISCORD_TWITCH_CHANNEL"));

        const liveMsgDiscrim = `${discordChannel.guild.id}_${discordChannel.name}_${streamData.id}`;

        let existingMsgId = messageHistory[liveMsgDiscrim] || null;

        if (existingMsgId) {
            discordChannel.messages.fetch(existingMsgId)
                .then((existingMsg) => {
                    existingMsg.edit({
                        embeds: [msgEmbed],
                        content: msgFormatted
                    }).then((message) => {
                       if (!isLive) {
                           delete messageHistory[liveMsgDiscrim];
                           liveMessageDb.put('history2', messageHistory);
                       }
                    });
                })
                .catch((e) => {

                    if (e.message === "Unkown Message") {
                        delete messageHistory[liveMsgDiscrim];
                        liveMessageDb.put('history2', messageHistory);
                    }
                });
        } else {
            if (isLive) {
                let discordMentions = config.get("DISCORD_MENTIONS");
                let mentionMode = (discordMentions && discordMentions[streamData.user_name.toLowerCase()]) || null;
                if (mentionMode) {
                    mentionMode = mentionMode.toLowerCase();

                    if (mentionMode === "everyone" || mentionMode === "here") {
                        // Reserved @ keywords for discord that can be mentioned directly as text
                        mentionMode = `@${mentionMode}`;
                    } else {
                        // Most likely a role that needs to be translated to <@&id> format
                        let roleData = discordChannel.guild.roles.cache.find((role) => {
                            return (role.name.toLowerCase() === mentionMode);
                        });

                        if (roleData) {
                            mentionMode = `<@&${roleData.id}>`;
                        } else {
                            console.log('[Discord]', `Cannot mention role: ${mentionMode}`,
                                `(does not exist on server ${discordChannel.guild.name})`);
                            mentionMode = null;
                        }
                    }
                }
                let msgToSend = msgFormatted;
                if (mentionMode) {
                    msgToSend = msgFormatted + ` ${mentionMode}`
                }
                let msgOptions = {
                    embeds: [msgEmbed],
                    content: msgToSend
                };
                discordChannel.send(msgOptions)
                    .then((message) => {
                        console.log('[Discord]', `Sent announce msg to #${discordChannel.name} on ${discordChannel.guild.name}`)

                        messageHistory[liveMsgDiscrim] = message.id;
                        liveMessageDb.put('history2', messageHistory);
                    })
                    .catch((err) => {
                        console.log('[Discord]', `Could not send announce msg to #${discordChannel.name} on ${discordChannel.guild.name}:`, err.message);
                    });
            } else {

            }

            anySent = true;
        }

        liveMessageDb.put('history2', messageHistory);
        return anySent;
    })

    StreamActivity.init(client);

    TwitchMonitor.onChannelOffline((streamData) => {
        // Update activity
        StreamActivity.setChannelOffline(streamData);
    });

    const body = {
        "add": [
            {"value": "from:Shadoune666", "tag": "from Shadoune666!!"},
            {"value": "from:vickypalami", "tag": "from vickypalami!!"},
            {"value": "from:Conterstine", "tag": "from Conterstine!!"},
            {"value": "from:renrize", "tag": "from renrize!!"},
            {"value": "from:CrisGreen95", "tag": "from CrisGreen95!!"},
            {"value": "from:Nuvia_OuO", "tag": "from Nuvia_OuO!!"}
        ]
    }

    const r = await twitterClient.post("tweets/search/stream/rules", body);

    listenForever(
        () => twitterClient.stream('tweets/search/stream'),
        (data) => sendDiscord(data)

    )

    async function sendDiscord(data) {

        const tweet = await twitterClient.get('tweets/'+data.id+"?tweet.fields=author_id,attachments,created_at,text&media.fields=url,preview_image_url&user.fields=profile_image_url,verified&expansions=author_id,attachments.media_keys");
        const msg = `https://twitter.com/${tweet.includes.users[0].username}/status/${tweet.data.id}`

        let verified = '';

        if (tweet.includes.users[0].verified == true) {
            verified = '🔵';
        }

        let msgEmbed = new EmbedBuilder()
            .setColor(0x00acee)
            .setAuthor(
                {
                    name: `${tweet.includes.users[0].name} (@${tweet.includes.users[0].username}) `+verified,
                    iconURL: tweet.includes.users[0].profile_image_url,
                    url: `https://twitter.com/${tweet.includes.users[0].username}`
                }
            )
            .setDescription(tweet.data.text)
            .setFooter(
                {
                    text: "Twitter",
                    iconURL: 'https://abs.twimg.com/favicons/twitter.2.ico'
                }
            )
            .setTimestamp(moment.now())
            .setURL(`https://twitter.com/${tweet.includes.users[0].username}/status/${tweet.data.id}`);

        if (tweet.includes.media) {
            msgEmbed.setImage(tweet.includes.media[0].url);
        }

        client.channels.cache.get(config.get("DISCORD_TWITTER_CHANNEL")).send({
            embeds: [msgEmbed],
            content: msg
        });
        return false;
    }

    client.on('guildMemberAdd', async member => {
        const welcomeChannel = client.channels.cache.get(config.get('DISCORD_WELCOME_CHANNEL_ID'));

        let welcomeCanvas = {};
        welcomeCanvas.create = Canvas.createCanvas(1024, 500);
        welcomeCanvas.context = welcomeCanvas.create.getContext('2d');

        await Canvas.loadImage("./src/img/bg.png").then(async (img) => {
            welcomeCanvas.context.drawImage(img, 0, 0, 1024, 500);
            welcomeCanvas.context.font = 'bold 72px Slant';
            welcomeCanvas.context.textAlign = 'center';
            welcomeCanvas.context.fillStyle = '#fda3df';
            welcomeCanvas.context.shadowColor = '#000000'
            welcomeCanvas.context.shadowBlur = 5;
            welcomeCanvas.context.fillText("BIENVENID@", 512, 360);
            welcomeCanvas.context.fillStyle = '#ffffff';
            welcomeCanvas.context.beginPath();
            welcomeCanvas.context.arc(512, 166, 128,0, Math.PI * 2, true);
            welcomeCanvas.context.stroke();
            welcomeCanvas.context.fill();
        });

        let canvas = welcomeCanvas;
        canvas.context.font = 'bold 42px Slant';
        canvas.context.fillStyle = '#ffffff';
        canvas.context.textAlign = 'center';
        canvas.context.shadowColor = '#000000'
        canvas.context.shadowBlur = 5;
        canvas.context.fillText(member.user.tag.toUpperCase(), 512, 410);
        canvas.context.beginPath();
        canvas.context.arc(512, 166, 119, 0, Math.PI * 2, true);
        canvas.context.closePath();
        canvas.context.clip();
        member.user.displayAvatarURL();

        await Canvas.loadImage("https://cdn.discordapp.com/avatars/" + member.user.id + "/" + member.user.avatar + ".png")
            .then(img => {
                canvas.context.drawImage(img, 393,47,238,238);
            });

        let attachment = new AttachmentBuilder(canvas.create.toBuffer(), {name: 'welcome-'+member.user.id+'.png'});

        const Embed = new EmbedBuilder()
            .setDescription('Hola '+member.user.username+"! Bienvenid@ 🐷")
            .setColor(0xfda3df)
            .setTitle('Un nuevo Sandwich de Jamon ha llegado!')
            .setImage('attachment://'+attachment.name)
            .setFooter({
                text: 'Gracias por unirte!',
            })

        welcomeChannel.send({embeds: [Embed], files: [attachment]});
        var sandwichitos = member.guild.roles.cache.find(role => role.name === "Sandwichitos");
        member.roles.add(sandwichitos);
        var minecraft = member.guild.roles.cache.find(role => role.name === "Minecraft");
        member.roles.add(minecraft);
    })

    client.on("messageCreate", async message => {
        if (message.channel.id == config.get("DISCORD_GPT_CHANNEL_ID") && !message.author.bot) {
            const user = message.author

            console.log("----Direct Message---")
            console.log("Date    : " + new Date())
            console.log("UserId  : " + user.id)
            console.log("User    : " + user.username)
            console.log("Message : " + message.content)
            console.log("--------------")

            try {
                let sentMessage = await message.reply({ content: `Pensando 🤔` });
                await askQuestion(message, async (response, id) => {
                    if (response.length >= MAX_RESPONSE_CHUNK_LENGTH) {
                        splitAndSendResponse(response, user, sentMessage)
                    } else {
                        await sentMessage.edit(response)
                    }
                })
            } catch (e) {
                console.error(e)
            }
        }
    })

    client.on("interactionCreate", async interaction => {
        switch (interaction.commandName) {
            case "ask":
                handle_interaction_ask(interaction)
                break;
            case "image":
                handle_interaction_image(interaction)
                break
            case "remix":
                handle_interaction_remix(interaction,client)
                break
        }
    });
}

async function listenForever(streamFactory, dataConsumer) {
    try {
        for await (const { data } of streamFactory()) {
            dataConsumer(data);

        }
        // The stream has been closed by Twitter. It is usually safe to reconnect.
        console.log('Stream disconnected healthily. Reconnecting.');
        listenForever(streamFactory, dataConsumer);
    } catch (error) {
        // An error occurred so we reconnect to the stream. Note that we should
        // probably have retry logic here to prevent reconnection after a number of
        // closely timed failures (may indicate a problem that is not downstream).
        console.warn('Stream disconnected with error. Retrying.', error);
        // listenForever(streamFactory, dataConsumer);
    }
}

class StreamActivity {
    /**
     * Registers a channel that has come online, and updates the user activity.
     */
    static setChannelOnline(stream) {
        this.onlineChannels[stream.user_name] = stream;
    }

    /**
     * Marks a channel has having gone offline, and updates the user activity if needed.
     */
    static setChannelOffline(stream) {
        delete this.onlineChannels[stream.user_name];
    }

    /**
     * Fetches the channel that went online most recently, and is still currently online.
     */
    static getMostRecentStreamInfo() {
        let lastChannel = null;
        for (let channelName in this.onlineChannels) {
            if (typeof channelName !== "undefined" && channelName) {
                lastChannel = this.onlineChannels[channelName];
            }
        }
        return lastChannel;
    }

    static init(discordClient) {
        this.onlineChannels = { };
    }
}
main()