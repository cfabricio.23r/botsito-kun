import {
    Client, REST, Partials,
    GatewayIntentBits, Routes,
    ActivityType, ChannelType, Attachment, AttachmentBuilder, Embed, EmbedBuilder
} from 'discord.js'
import moment from 'moment';
import humanizeDuration from "humanize-duration";
import config from '../config/config.js'

export default class LiveEmbed {
    static createForStream(streamData) {
        const isLive = streamData.type === "live";
        const allowBoxArt = config.get("TWITCH_USE_BOXART");

        let msgEmbed = new EmbedBuilder();
        msgEmbed.setColor(isLive ? "#9146ff" : "GREY");
        msgEmbed.setURL(`https://twitch.tv/${(streamData.login || streamData.user_name).toLowerCase()}`);

        // Thumbnail
        let thumbUrl = streamData.profile_image_url;

        if (allowBoxArt && streamData.game && streamData.game.box_art_url) {
            thumbUrl = streamData.game.box_art_url;
            thumbUrl = thumbUrl.replace("{width}", "288");
            thumbUrl = thumbUrl.replace("{height}", "384");
        }

        msgEmbed.setThumbnail(thumbUrl);

        let fields = {};

        if (isLive) {
            // Title
            msgEmbed.setTitle(`:red_circle: **${streamData.user_name} esta en vivo en Twitch!**`);
            msgEmbed.addFields({name: "Titulo", value: streamData.title, inline: false});
        } else {
            msgEmbed.setTitle(`:white_circle: ${streamData.user_name} estuvo en vivo en Twitch.`);
            msgEmbed.setDescription('El directo ha terminado.');
            msgEmbed.addFields({name: "Titulo", value: streamData.title, inline: true});
        }

        // Add game
        if (streamData.game) {
            msgEmbed.addFields({name: "Juego", value: streamData.game.name, inline: false});
        }

        if (isLive) {
            // Add status
            msgEmbed.addFields({name: "Estado", value: isLive ? `En vivo con ${streamData.viewer_count} viewers` : 'El directo ha terminado', inline: true});

            // Set main image (stream preview)
            let imageUrl = streamData.thumbnail_url;
            imageUrl = imageUrl.replace("{width}", "1280");
            imageUrl = imageUrl.replace("{height}", "720");
            let thumbnailBuster = (Date.now() / 1000).toFixed(0);
            imageUrl += `?t=${thumbnailBuster}`;
            msgEmbed.setImage(imageUrl);

            // Add uptime
            let now = moment();
            let startedAt = moment(streamData.started_at);

            msgEmbed.addFields({name: "Tiempo", value: humanizeDuration(now - startedAt, {
                    delimiter: ", ",
                    largest: 2,
                    round: true,
                    units: ["y", "mo", "w", "d", "h", "m"]
                }), inline: true});

        }
        return msgEmbed;
    }
}