import admin from 'firebase-admin';
import Keyv from 'keyv';
import KeyvFirestore from 'keyv-firestore';
import firebaseServiceAccount from '../configFile/firebaseServiceAccountKey.json' assert {type: 'json'}

export async function initFirebaseAdmin() {
    admin.initializeApp({
        credential: admin.credential.cert(firebaseServiceAccount),
        databaseURL: `https://${firebaseServiceAccount.project_id}.firebaseio.com`
    });
    const db = admin.firestore();
    return db;
}

export async function initKeyvFirestore() {
    const messageStore = new Keyv({
        store: new KeyvFirestore({
            projectId: firebaseServiceAccount.project_id,
            collection: 'messageStore',
            credentials: firebaseServiceAccount
        })
    });
    return messageStore;
}