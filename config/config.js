import fs from "fs/promises"
const PATH = "./configFile/config.json"
let config = {
    "OPENAI_MODEL":"gpt-3.5-turbo",
    "MAX_TOKEN":1900,
    "ENABLE_DIRECT_MESSAGES":false,
    "CONVERSATION_START_PROMPT":"",
    "USE_EMBED":true,
    "DEFAULT_ACTIVITY":"ser un subnormal",
    "DISCORD_GPT_CHANNEL_ID":"1089361521190125628",
    "DISCORD_WELCOME_CHANNEL_ID":"1089224188914585621",
    "TWITCH_CHANNELS": "nuvia_ouo, vickypalami, shadounne",
    "DISCORD_TWITCH_CHANNEL": "1089224188914585621",
    "DISCORD_MENTIONS": {
        "nuvia_ouo": "everyone",
        "vickypalami": "everyone",
        "shadoune666": "everyone"
    },
    "TWITCH_USE_BOXART": true,
    "TWITCH_OAUTH_TOKEN": "p6lyz64vvzouk9b4mh2legaph9ynyd",
    "TWITCH_CLIENT_ID": "gp762nuuoqcoxypju8c569th9wz7q5",
    "TWITCH_CHECK_INTERVAL_MS": 60000,
    "TWITTER_ACCESS_TOKEN_KEY": "F8VRrKOOmJpuj4xw1xh9ZLatyn6tb5BZdIcRQmDAqNbf2",
    "TWITTER_ACCESS_TOKEN_SECRET": "261509382-38dloKib3ebxC0SaPE2Lb6aywZOg2FBfGYbqr4KV",
    "TWITTER_CONSUMER_SECRET": "FFtsHI190CEvG8Dk7w0TiQK8v9mLZ5HMj3YBKIvwDHdKry4iTc",
    "TWITTER_CONSUMER_KEY": "QnVy7jXohOu6cV0MLZYsPSuUk",
    "DISCORD_TWITTER_CHANNEL": "1089738893877858456",
    "TWITTER_ACCOUNTS": "261509382,743896424,334236519,1375877599,994685038242381825,922584181087113216",
    "TWITTER_BEARER_TOKEN": "AAAAAAAAAAAAAAAAAAAAAE7WTAEAAAAACUvwUaGhBEI5qqFAtxgQZXWTqLQ%3DxdCbesV5zByYy12QH3OGdGW3svolF47JKZ420gjY8AW4sX1onA"
}

load()

function get(key){
    return config[key]
}

function set(key,value){
    if(config[key] == undefined){
        console.log("Invalid config key : ",key)
        return;
    }
    config[key] = value
}

function save(){
    fs.writeFile(PATH,JSON.stringify(config)).then(()=>{
        console.log("Config saved!")
    }).catch((err)=>{
        console.error("Config save error! : ",err)
    })
}

function load(){
    fs.readFile(PATH).then((data)=>{
        try{
            data = data.toString()
            config = JSON.parse(data)
            console.log(new Date()+"   ---    Config loaded")
        }catch(e){
            console.error("Config file corrupted! : ",e)
        }
    }).catch(()=>{
        save()
    })
}

function getFullConfig(){
    return {...config}
}

export default {
    save,
    load,
    set,
    get,
    getFullConfig
}