import config from '../config/config.js'
import tokenCount from './tokenCount.js'
import { ChatGPTAPI } from 'chatgpt';
import {initFirebaseAdmin, initKeyvFirestore} from '../config/db.js'

const messageStore = await initKeyvFirestore();

const db = await initFirebaseAdmin();

async function initOpenAI(messageStore) {
    const api = new ChatGPTAPI({
        apiKey: process.env.OPENAI_API_KEY,
        messageStore,
        debug: true
    });
    return api;
}
const chatGPT = await initOpenAI(messageStore).catch(error => {
    console.error(error);
    process.exit();
});

/*
chatGPT.sendMessage = async function (prompt) {

    const tokens = tokenCount(prompt)
    const MAX_TOKENS = config.get("MAX_TOKEN")

    if (tokens > MAX_TOKENS / 2) {
        return `Please limit your prompt to a maximum of ${parseInt(MAX_TOKENS / 2)} tokens. Thank you.`
    }

    const messages = [
        {
            role: "system",
            content: config.get("CONVERSATION_START_PROMPT") != "" ?
                config.get("CONVERSATION_START_PROMPT") :
                "You are helpful assistant"
        },
        {
            role: "user",
            content: prompt
        }
    ]

    const data = {
        model: config.get("OPENAI_MODEL"),
        messages,
        max_tokens: MAX_TOKENS - tokens
    }

    let res = await fetch("https://api.openai.com/v1/chat/completions",
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${process.env.OPENAI_API_KEY}`
            },
            body: JSON.stringify(data)
        })
    res = await res.json()
    if(res.error){
        console.error(res)
    }

    return {
        id: res.id,
        text: res.choices[0].message.content.trim(),
        usage: res.usage,
        tokens
    }
}
*/

async function saveChat(user, message, response, id){
    const timeStamp = new Date();
    const date = timeStamp.getUTCDate().toString() + '.' + timeStamp.getUTCMonth().toString() + '.' + timeStamp.getUTCFullYear().toString();
    const time = timeStamp.getUTCHours().toString() + ':' + timeStamp.getUTCMinutes().toString() + ':' + timeStamp.getUTCSeconds().toString();
    await db.collection('chat-history').doc(user.id)
        .collection(date).doc(time).set({
            timeStamp: new Date(),
            userID: user.id,
            user: user.tag,
            question: message.content,
            answer: response,
            parentMessageId: id,
        });
}
export async function askQuestion(message, cb, opts = {}) {
    let prompt = config.get("CONVERSATION_START_PROMPT");
    try {
        const doc = await db.collection('users').doc(message.author.id).get();
        if (!doc.exists) {
            chatGPT.sendMessage(message.content,{
                systemMessage: prompt,
            }).then((res)=> {
                db.collection('users').doc(message.author.id).set({
                    userId: message.author.id,
                    user: message.author.tag,
                    parentMessageId: res.id,
                });
                saveChat(message.author, message, res.text, res.id);
                console.log(res);
                cb(res.text, res.id);
                console.log(res);
            });
        } else {
            chatGPT.sendMessage(message.content,{
                systemMessage: prompt,
                parentMessageId: doc.data().parentMessageId
            }).then((res)=> {
                db.collection('users').doc(message.author.id).set({
                    userId: message.author.id,
                    user: message.author.tag,
                    parentMessageId: res.id,
                });
                saveChat(message.author, message, res.text, res.id);
                cb(res.text, res.id);
                console.log(res);
            });
        }
    } catch (e) {
        cb("Oppss, something went wrong! (Error)", null)
        console.error("chat error : " + e)
    }
}